;;; publish.el --- Publish reveal.js presentation, HTML, and PDF from Org sources
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2017-2021 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;; Mostly, settings from emacs-reveal-publish are used.
;; Besides, non-free logos are published here.
;;
;; Use this file from its parent directory with the following shell
;; command:
;; emacs --batch --load elisp/publish.el

;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t)

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir)
  (condition-case nil
      ;; Either require package with above hard-coded location
      ;; (e.g., in docker) ...
      (require 'emacs-reveal)
    (error
     ;; ... or look for sibling "emacs-reveal" of oer-courses.
     (add-to-list
      'load-path
      (expand-file-name "../../../oer/emacs-reveal/" (file-name-directory load-file-name)))
     (require 'emacs-reveal))))

(oer-reveal-publish-all
 (list
  (list "images"
	:base-directory "img"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/img"
	:publishing-function 'org-publish-attachment)
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/css/theme"
	:publishing-function 'org-publish-attachment)))

(provide 'publish)
;;; publish.el ends here
